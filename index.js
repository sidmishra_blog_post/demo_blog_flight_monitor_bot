const puppeteer = require('puppeteer-extra');
const stealthPlugin = require('puppeteer-extra-plugin-stealth');
puppeteer.use(stealthPlugin());

(async () => {
    const browser = await puppeteer.launch({headless: false, 
        args: ['--incognito', '--start-maximized'],
        executablePath: 'C:/Program Files/Google/Chrome/Application/chrome.exe' 
    });
    const [page] = await browser.pages();
    await page.setViewport({width: 1280, height: 1000});
    await page.goto("https://www.makemytrip.com/flight/search?itinerary=PNQ-BBI-18/01/2023_BBI-PNQ-31/01/2023&tripType=R&paxType=A-1_C-0_I-0&intl=false&cabinClass=E&ccde=IN&lang=eng");
    await page.waitForSelector('#listing-id > .splitVw > .paneView:nth-child(2) .listingCardWrap label');
    await page.waitForSelector(".filterWrapper > .filtersOuter:nth-child(1) span[title='Non Stop']", { visible: true });
    await page.click(".filterWrapper > .filtersOuter:nth-child(1) span[title='Non Stop']");
    await page.waitForSelector(".listingCard > div:nth-child(1)");
    const airlineCollection = await page.$$(".splitVw label.splitViewListing > div > div > div:nth-child(1) > span");
    const departureTimesCollection = await page.$$(".splitVw label.splitViewListing > div > div > div:nth-child(1) > div:nth-child(1) > p > span");
    const arrivaltimesCollection = await page.$$(".splitVw label.splitViewListing > div > div > div:nth-child(1) > div:nth-child(3) > p > span");
    const priceCollection = await page.$$(".splitVw label.splitViewListing > div > div:nth-child(2) > div:nth-child(2) > div > p");
    console.log("Airline | Departure Time | Arrival Time | Price");
    for(let i=0; i<airlineCollection.length; i++){
        let tempAirline = airlineCollection[i];
        let tempDepartTime = departureTimesCollection[i];
        let tempArrivalTime = arrivaltimesCollection[i];
        let tempPrice = priceCollection[i];
        let flightName = await page.evaluate(el => el.textContent, tempAirline);
        let departureTime = await page.evaluate(el => el.textContent, tempDepartTime);
        let arrivalTime = await page.evaluate(el => el.textContent, tempArrivalTime);
        let price = await page.evaluate(el => el.textContent, tempPrice);
        console.log(flightName+" | "+departureTime+" | "+arrivalTime+" | "+price);
    }
    await browser.close();
})();
