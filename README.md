# Chromium based basic bot to check flight prices

## Setup
Make sure you have NodeJS 10+ installed in your machine. Clone this repo to your file system and execute command npm install.

## Run locally
Run `npm run start`

##Arguments
Substitute the URL part - https://www.makemytrip.com/flight/search?itinerary=PNQ-BBI-18/01/2023_BBI-PNQ-31/01/2023 with your 
travel date and city codes. In this case city codes are PNQ for pune and BBI for Bhubaneswar
